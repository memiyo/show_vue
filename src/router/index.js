import { createRouter, createWebHistory } from 'vue-router'
import DefaultView from '../views/Default.vue'
import HomeView from '../views/HomeView.vue'
import SecondaryComponent from '../views/SecondaryComponent.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: DefaultView
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: SecondaryComponent
    }
  ]
})

export default router
