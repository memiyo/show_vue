import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createAuth0 } from "@auth0/auth0-vue";

import {store} from './store'

import './assets/main.css'

const app = createApp(App)

app.use(router)
app.use(store)
app.use(
    createAuth0({
      domain: import.meta.env.VITE_AUTH0_DOMAIN,
      client_id: import.meta.env.VITE_AUTH0_CLIENT_ID,
      redirect_uri: import.meta.env.VITE_AUTH0_CALLBACK_URL,
    })
  )
app.directive("theme", {
    mounted(el, binding) {
        if (binding.value === 'primary') {
            el.style.color = 'red'
          } else if (binding.value === 'secondary') {
            el.style.color = 'green'
          } else if (binding.value === 'tertiary') {
            el.style.color = 'blue'
          } else {
            el.style.color = 'black'
          }
    }})
app.directive("validate", {
    mounted(el, binding) {
        if (binding.value === "") {
            el.placeholder = "Ingrese un valor"
        }
    },
    updated(el, binding) {
        if (binding.value === "") {
            el.placeholder = "Ingrese un valor"
        }
        else if (binding.value === "black") {
            el.style.color = 'black'
        }
        else if (binding.value === "red") {
            el.style.color = 'red'
        }
    }})
app.mount('#app')
