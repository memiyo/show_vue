import { createStore } from 'vuex'

export const store = createStore({
  state () {
    return {
      count: 0,
      name: "defaultName",
      mail: "default@mail.com"
    }
  },
  mutations: {
    increment (state) {
      state.count++
    },
    modifyProfile(state,payload){
      state.name = payload.profileName
      state.mail = payload.profileMail
    }
  }
})
